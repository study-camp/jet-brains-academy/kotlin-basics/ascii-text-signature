import signature.Font
import signature.FontFile
import signature.Signature

fun main() {
    val name = readLine()!!
    val status = readLine()!!
    val roman = Font(FontFile.ROMAN)
    val medium = Font(FontFile.MEDIUM)
    val signature = Signature(roman.formatText(name), medium.formatText(status))
    print(signature.getFrame())
}
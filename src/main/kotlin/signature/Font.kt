package signature

import kotlin.properties.Delegates

const val EXTRA_STEP = 1

class Font(fontName: FontFile) {
    private var font = mutableMapOf<String, List<String>>()
    private var charSize by Delegates.notNull<Int>()
    private var fontSize by Delegates.notNull<Int>()

    init {
        val rawFont = readFontFile(fontName.value)
        val sizes = rawFont[0].split(" ").map { it.toInt() }
        charSize = sizes.first()
        fontSize = sizes.last()
        val stepSize = charSize + EXTRA_STEP
        val rangeEnd = fontSize * stepSize
        for (charIndex in 1..rangeEnd step stepSize) {
            val (letter, width) = rawFont[charIndex].split(" ")
            font[letter] = List(charSize) {
                val index = charIndex + EXTRA_STEP + it
                rawFont[index].take(width.toInt())
            }
        }
        fillWithSpaces(fontName.spaceSize)
    }

    fun formatText(text: String): String {
        val list = MutableList(charSize) { "" }

        text.forEach {
            list.indices.forEach { i ->
                list[i] = list[i] + font["$it"]!![i]
            }
        }
        return list.joinToString("\n") {
            it.dropLast(1)
        }
    }

    private fun readFontFile(fontName: String): List<String> {
        val fontFile = FontFile::class.java.getResource("/$fontName.txt").readText();
        return fontFile.split("\n")
    }

    private fun fillWithSpaces(width: Int) {
        font[" "] = List(charSize) { " ".repeat(width) }
    }
}
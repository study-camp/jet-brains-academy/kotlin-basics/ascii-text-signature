package signature
const val IN_HALF = 2
const val EXTRA_SPACE = 2
class Signature(private var nameTag: String, private var status: String) {
    private var horizontalRuler = ""
    init {
        frameIt()
    }

    fun getFrame(): String {
        return "$horizontalRuler\n$nameTag\n$status\n$horizontalRuler"
    }

    private fun frameIt(){
        val nameTagWidth = nameTag.indexOfFirst{ it == '\n' }
        val statusWidth = status.indexOfFirst { it == '\n' }

        if (nameTagWidth >= statusWidth) {
            nameTag = surroundIt(nameTag)
            status = surroundIt(centerIt(status, nameTagWidth))
        } else {
            nameTag = surroundIt(centerIt(nameTag, statusWidth))
            status = surroundIt(status)
        }
        horizontalRuler = "8".repeat(nameTag.indexOfFirst{ it == '\n' })
    }
    private fun surroundIt(text: String): String {
        val splited = text.split("\n")
        return  splited.joinToString("\n") {
            "88  $it   88"
        }
    }
    private fun centerIt(text: String, width: Int): String {
        val splited = text.split("\n")
        val half = width / IN_HALF - splited.first().length / IN_HALF
        val removeSpace = if (width % IN_HALF - splited.first().length % IN_HALF == -1) -1 else 0
        val left = " ".repeat(half + removeSpace)
        val right = " ".repeat(half + if(half * 2 + splited.first().length < width) 1 else 0)

        return  splited.joinToString("\n") {
            "$left$it$right"
        }
    }
}
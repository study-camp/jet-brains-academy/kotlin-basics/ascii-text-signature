package signature

enum class FontFile(val value: String, val spaceSize: Int) {
    ROMAN("roman", 10),
    MEDIUM("medium", 5)
}
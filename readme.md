# ASCII Text Signature
https://hyperskill.org/projects/71?track=18

When learning a new programming language, we always have to figure out how to print text data. It is a simple and useful skill: you can print texts everywhere, even in the console. There is only one problem: the text itself isn’t pretty enough for your taste. What if we try to add a little “make-up”? Or get very creative with fonts, draw awesome letters with other letters? Let’s try this out.

![Sample Video](https://stepik.org/media/attachments/lesson/226140/demonstration.mp4)